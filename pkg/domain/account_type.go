package domain

import (
	"encoding/json"
	"errors"
	"fmt"
	"slices"
)

var ErrInvalidAccountType = errors.New("invalid account type")

type AccountType struct {
	id         string
	nominalCap bool
}

var (
	GIA  = AccountType{id: "GIA", nominalCap: false}
	ISA  = AccountType{id: "ISA", nominalCap: true}
	SIPP = AccountType{id: "SIPP", nominalCap: true}
)

// AccountTypes lists all of the available AccountTypes,
// in the order of allocation preference to maximise tax
// advantage
var AccountTypes = []AccountType{
	ISA,
	SIPP,
	GIA,
}

func (t *AccountType) String() string {
	return string(t.id)
}

// ParseAccountType attempts to return a predefined AccountType object identified by the string.
// Valid acount types: "GIA", "ISA", "SIPP"
// Possible errors:
//   - ErrInvalidAccountType: when the string to parse does not match one of the above listed values
func ParseAccountType(s string) (AccountType, error) {
	idx := slices.IndexFunc(AccountTypes, func(t AccountType) bool { return t.id == s })
	if idx < 0 {
		return AccountType{}, ErrInvalidAccountType
	}
	return AccountTypes[idx], nil
}

func (t *AccountType) UnmarshalJSON(b []byte) error {
	s := ""
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}

	*t, err = ParseAccountType(s)
	if err != nil {
		return NewValidationErr(fmt.Sprintf(
			"invalid account type '%s': must be one of %v",
			s,
			AccountTypes,
		))
	}

	return nil
}

func (t AccountType) MarshalJSON() ([]byte, error) {
	return json.Marshal(t.id)
}

func (t *AccountType) IsCapped() bool {
	return t.nominalCap
}
