package domain

import "errors"

var (
	ErrNotFound                   = errors.New("not found")
	ErrIDAlreadyExists            = errors.New("id already exists")
	ErrUnknownClientID            = errors.New("unknown client id")
	ErrCannotAllocateReceiptFunds = errors.New("cannot allocate receipt funds")
)

type ErrInvalidData struct {
	msg string
}

func NewValidationErr(msg string) ErrInvalidData {
	return ErrInvalidData{
		msg: msg,
	}
}

func (err ErrInvalidData) Error() string {
	return err.msg
}
