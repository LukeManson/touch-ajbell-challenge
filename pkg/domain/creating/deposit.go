package creating

import (
	"context"
	"fmt"
	"slices"

	"github.com/google/uuid"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain/retrieving"
)

type Deposit struct {
	Pots     []Pot     `json:"pots"`
	ClientID uuid.UUID `json:"client_id"`
}

type Pot struct {
	Name     string    `json:"name"`
	Accounts []Account `json:"accounts"`
}

type Account struct {
	Type          domain.AccountType `json:"type"`
	NominalAmount int                `json:"nominal_amount"`
}

type depositCreator interface {
	CreateDeposit(ctx context.Context, deposit Deposit) (retrieving.Deposit, error)
}

type Service struct {
	repository depositCreator
}

// NewService creates a new service using an implementation of the depositCreator interface
func NewService(repo depositCreator) Service {
	return Service{
		repository: repo,
	}
}

// ConfigureNewDeposit creates a new deposit with balance 0
// Possible errors:
//   - ErrInvalidData: when the configuration of the new deposit is invalid. e.g. multiple of he same account type in one pot
func (s *Service) ConfigureNewDeposit(ctx context.Context, d Deposit) (retrieving.Deposit, error) {
	err := d.validate()
	if err != nil {
		return retrieving.Deposit{}, err
	}

	return s.repository.CreateDeposit(ctx, d)
}

func (d Deposit) validate() error {
	for _, p := range d.Pots {
		err := p.validate()
		if err != nil {
			return err
		}
	}

	return nil
}

func (p Pot) validate() error {
	nameLen := len(p.Name)
	if nameLen < 1 || nameLen > 64 {
		return domain.NewValidationErr(fmt.Sprintf(
			"invalid pot name '%s': pot names must be between 1 and 64 characters in length",
			p.Name,
		))
	}

	if len(p.Accounts) < 1 {
		return domain.NewValidationErr("invalid number of pot accounts: each pot must have at least one account")
	}

	accountTypeList := []domain.AccountType{}
	for _, a := range p.Accounts {
		err := a.validate()
		if err != nil {
			return err
		}

		// Check that the account types are all unique
		if slices.Contains(accountTypeList, a.Type) {
			return domain.NewValidationErr(fmt.Sprintf(
				"invalid pot accounts: each account can only contain one of each account type: multiple instances of %s",
				a.Type.String(),
			))
		}
		accountTypeList = append(accountTypeList, a.Type)
	}

	return nil
}

func (a Account) validate() error {
	if a.NominalAmount < 1 {
		return domain.NewValidationErr("invalid nominal amount: must be a positive integer")
	}

	return nil
}
