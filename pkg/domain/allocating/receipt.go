package allocating

import (
	"context"
	"slices"

	"github.com/google/uuid"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain/retrieving"
)

type Receipt struct {
	PotID uuid.UUID `json:"pot_id"`
	Value int       `json:"value"`
}

type AccountAllocation struct {
	Type       domain.AccountType
	Allocation int
}

type receiptCreator interface {
	CreateReceipt(ctx context.Context, depositID uuid.UUID, receipt Receipt, allocations []AccountAllocation) (retrieving.Receipt, error)
	GetDepositByID(ctx context.Context, clientID uuid.UUID, depositID uuid.UUID) (retrieving.Deposit, error)
}

type Service struct {
	repository receiptCreator
}

// NewService creates a new service using an implementation of the depositCreator interface
func NewService(repo receiptCreator) Service {
	return Service{
		repository: repo,
	}
}

// AllocateReceipt distributes a receipts funds to a pot's accounts in tax advantage priority
// Possible errors:
//   - ErrNotFound: when the specified deposit, or pot does not exist
//   - ErrCannotAllocateReceiptFunds: When the receipt has higher value than the remaining available
//     balance of the ISA and SIPP accounts, and there is no GIA account to overflow into.
func (s *Service) AllocateReceipt(
	ctx context.Context,
	receipt Receipt,
	clientID uuid.UUID,
	depositID uuid.UUID,
) (retrieving.Receipt, error) {
	deposit, err := s.repository.GetDepositByID(ctx, clientID, depositID)
	if err != nil {
		return retrieving.Receipt{}, err
	}

	potIdx := slices.IndexFunc(deposit.Pots, func(p retrieving.Pot) bool {
		return p.ID == receipt.PotID
	})
	if potIdx == -1 {
		return retrieving.Receipt{}, domain.ErrNotFound
	}

	pot := deposit.Pots[potIdx]
	remainingValue := receipt.Value
	allocations := []AccountAllocation{}

	for _, accountType := range domain.AccountTypes {
		accountIdx := slices.IndexFunc(pot.Accounts, func(a retrieving.Account) bool {
			return a.Type == accountType
		})
		if accountIdx == -1 {
			continue
		}
		account := pot.Accounts[accountIdx]

		availableBalance := account.NominalAmount - account.Balance
		if availableBalance <= 0 {
			continue
		}

		if remainingValue > availableBalance && accountType.IsCapped() {
			allocations = append(allocations, AccountAllocation{
				Type:       accountType,
				Allocation: availableBalance,
			})
			remainingValue -= availableBalance
			continue
		}

		allocations = append(allocations, AccountAllocation{
			Type:       accountType,
			Allocation: remainingValue,
		})
		remainingValue = 0
	}

	if remainingValue > 0 {
		return retrieving.Receipt{}, domain.ErrCannotAllocateReceiptFunds
	}

	storedReceipt, err := s.repository.CreateReceipt(ctx, depositID, receipt, allocations)
	if err != nil {
		return retrieving.Receipt{}, err
	}

	return storedReceipt, nil
}
