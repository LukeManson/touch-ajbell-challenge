package retrieving

import (
	"context"

	"github.com/google/uuid"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain"
)

type Deposit struct {
	Pots     []Pot     `json:"pots"`
	Receipts []Receipt `json:"receipts"`
	ClientID uuid.UUID `json:"client_id"`
	ID       uuid.UUID `json:"id"`
}

type Pot struct {
	Name     string    `json:"name"`
	Accounts []Account `json:"accounts"`
	ID       uuid.UUID `json:"id"`
}

type Account struct {
	Type          domain.AccountType `json:"type"`
	NominalAmount int                `json:"nominal_amount"`
	Balance       int                `json:"balance"`
}

type Receipt struct {
	AccountAllocations []AccountAllocation `json:"allocations"`
	Value              int                 `json:"value"`
	PotID              uuid.UUID           `json:"potID"`
	ID                 uuid.UUID           `json:"id"`
}

type AccountAllocation struct {
	Type       domain.AccountType `json:"account"`
	Allocation int                `json:"value"`
}

type depositRetriever interface {
	GetDepositByID(ctx context.Context, clientID uuid.UUID, depositID uuid.UUID) (Deposit, error)
}

type Service struct {
	repository depositRetriever
}

func NewService(repo depositRetriever) Service {
	return Service{
		repository: repo,
	}
}

// GetDepositByID gets a deposit from storage that matches the clientID and depositID
// Possible errors:
//   - ErrNotFound: if the deposit does not exist or is not owned by the client
func (s *Service) GetDepositByID(ctx context.Context, clientID uuid.UUID, depositID uuid.UUID) (Deposit, error) {
	return s.repository.GetDepositByID(ctx, clientID, depositID)
}
