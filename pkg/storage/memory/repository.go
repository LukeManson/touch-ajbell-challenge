package memory

import (
	"context"
	"slices"

	"github.com/google/uuid"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain/allocating"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain/creating"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain/retrieving"
)

type client struct {
	id   uuid.UUID
	name string
}

type store struct {
	deposits []deposit
	receipts []receipt
	clients  []client
}

func NewStore() *store {
	return &store{
		deposits: []deposit{},
	}
}

// SeedClient inserts client data into the store.
// It is used for the purpose of testing and demonstration since
// the scope of this task does not include client creation.
func (s *store) SeedClient(clientID uuid.UUID, clientName string) error {
	if slices.ContainsFunc(s.clients, func(c client) bool {
		return c.id == clientID
	}) {
		return domain.ErrIDAlreadyExists
	}
	s.clients = append(s.clients, client{id: clientID, name: clientName})
	return nil
}

// GetDepositByID gets a deposit from the store that matches the supplied ID
// Possible errors:
//   - ErrNotFound: when the deposit does not exist in the store
func (s *store) GetDepositByID(ctx context.Context, clientID uuid.UUID, depositID uuid.UUID) (retrieving.Deposit, error) {
	// For this simple in-memory implementation I'm just looping over all the records to find a match.
	deposit, idx := find(s.deposits, func(d deposit) bool { return d.id == depositID && d.clientID == clientID })
	if idx < 0 {
		return retrieving.Deposit{}, domain.ErrNotFound
	}

	matchingReceipts := []receipt{}
	for _, r := range s.receipts {
		if r.depositID == depositID {
			matchingReceipts = append(matchingReceipts, r)
		}
	}

	return appendReceiptsToDeposit(deposit.toRetrieving(), matchingReceipts), nil
}

// CreateDeposit adds a new desposit to the store
// Possible errors:
//   - ErrUnknownClientID: when the client ID does not match a client record in the store
func (s *store) CreateDeposit(ctx context.Context, deposit creating.Deposit) (retrieving.Deposit, error) {
	d := newDeposit(deposit)

	// Check the client exists
	if !slices.ContainsFunc(s.clients, func(c client) bool {
		return c.id == d.clientID
	}) {
		return retrieving.Deposit{}, domain.ErrUnknownClientID
	}

	s.deposits = append(s.deposits, d)

	return d.toRetrieving(), nil
}

// CreateReceipt adds a new receipt to the store and allocates the specified funds to the corresponding accounts
// Possible errors:
//   - ErrNotFound: when a specified deposit, pot, or account does not exist
func (s *store) CreateReceipt(ctx context.Context, depositID uuid.UUID, receipt allocating.Receipt, allocations []allocating.AccountAllocation) (retrieving.Receipt, error) {
	// In a database environment where transactions are available, this would be done in a transaction.
	// If something went wrong, we can then roll back.

	// Update balances
	err := s.UpdateAccountBalances(ctx, depositID, receipt.PotID, allocations)
	if err != nil {
		return retrieving.Receipt{}, err
	}

	// Create receipt
	r := newReceipt(depositID, receipt, allocations)
	s.receipts = append(s.receipts, r)

	return r.toRetrieving(), nil
}

func (s *store) UpdateAccountBalances(ctx context.Context, depositID uuid.UUID, potID uuid.UUID, allocations []allocating.AccountAllocation) error {
	deposit, depositIdx := find(s.deposits, func(d deposit) bool { return d.id == depositID })
	if depositIdx < 0 {
		return domain.ErrNotFound
	}

	pot, potIdx := find(deposit.pots, func(p pot) bool { return p.id == potID })
	if potIdx < 0 {
		return domain.ErrNotFound
	}

	for _, alloc := range allocations {
		account, accIdx := find(pot.accounts, func(a account) bool { return a.typ == alloc.Type })
		if accIdx < 0 {
			return domain.ErrNotFound
		}
		account.balance += alloc.Allocation
		pot.accounts[accIdx] = account
	}

	deposit.pots[potIdx] = pot
	s.deposits[depositIdx] = deposit
	return nil
}

func find[S ~[]E, E any](s S, f func(E) bool) (E, int) {
	var v E
	idx := slices.IndexFunc(s, f)
	if idx < 0 {
		return v, idx
	}

	return s[idx], idx
}
