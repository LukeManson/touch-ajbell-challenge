package memory

import (
	"github.com/google/uuid"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain/allocating"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain/creating"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain/retrieving"
)

type deposit struct {
	pots     []pot
	clientID uuid.UUID
	id       uuid.UUID
}

type pot struct {
	name     string
	accounts []account
	id       uuid.UUID
}

type account struct {
	typ           domain.AccountType
	nominalAmount int
	balance       int
}

type accountAllocation struct {
	typ        domain.AccountType
	allocation int
}

type receipt struct {
	allocations []accountAllocation
	value       int
	id          uuid.UUID
	depositID   uuid.UUID
	potID       uuid.UUID
}

func newDeposit(d creating.Deposit) deposit {
	id := uuid.New()
	pots := []pot{}
	for _, p := range d.Pots {
		pots = append(pots, newPot(p))
	}
	return deposit{
		id:       id,
		clientID: d.ClientID,
		pots:     pots,
	}
}

func newPot(p creating.Pot) pot {
	id := uuid.New()
	accounts := []account{}
	for _, a := range p.Accounts {
		accounts = append(accounts, newAccount(a))
	}
	return pot{
		id:       id,
		name:     p.Name,
		accounts: accounts,
	}
}

func newReceipt(depositID uuid.UUID, r allocating.Receipt, allocations []allocating.AccountAllocation) receipt {
	id := uuid.New()
	storeAllocations := []accountAllocation{}
	for _, a := range allocations {
		storeAllocations = append(storeAllocations, accountAllocation{
			typ:        a.Type,
			allocation: a.Allocation,
		})
	}
	return receipt{
		id:          id,
		depositID:   depositID,
		potID:       r.PotID,
		value:       r.Value,
		allocations: storeAllocations,
	}
}

func newAccount(a creating.Account) account {
	return account{
		typ:           a.Type,
		nominalAmount: a.NominalAmount,
		balance:       0,
	}
}

func (a account) toRetrieving() retrieving.Account {
	return retrieving.Account{
		Type:          a.typ,
		NominalAmount: a.nominalAmount,
		Balance:       a.balance,
	}
}

func (p pot) toRetrieving() retrieving.Pot {
	accounts := []retrieving.Account{}
	for _, a := range p.accounts {
		accounts = append(accounts, a.toRetrieving())
	}
	return retrieving.Pot{
		ID:       p.id,
		Name:     p.name,
		Accounts: accounts,
	}
}

func (d deposit) toRetrieving() retrieving.Deposit {
	pots := []retrieving.Pot{}
	for _, a := range d.pots {
		pots = append(pots, a.toRetrieving())
	}
	return retrieving.Deposit{
		ID:       d.id,
		ClientID: d.clientID,
		Pots:     pots,
		Receipts: []retrieving.Receipt{},
	}
}

func appendReceiptsToDeposit(deposit retrieving.Deposit, receipts []receipt) retrieving.Deposit {
	for _, r := range receipts {
		deposit.Receipts = append(deposit.Receipts, r.toRetrieving())
	}

	return deposit
}

func (r receipt) toRetrieving() retrieving.Receipt {
	allocations := []retrieving.AccountAllocation{}
	for _, a := range r.allocations {
		allocations = append(allocations, a.toRetrieving())
	}

	return retrieving.Receipt{
		ID:                 r.id,
		Value:              r.value,
		PotID:              r.potID,
		AccountAllocations: allocations,
	}
}

func (a accountAllocation) toRetrieving() retrieving.AccountAllocation {
	return retrieving.AccountAllocation{
		Type:       a.typ,
		Allocation: a.allocation,
	}
}
