package protobuf

import (
	"context"
	"errors"

	"github.com/google/uuid"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain/allocating"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain/creating"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain/retrieving"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

type Server struct {
	UnimplementedDepositsServer
	creatingService   creating.Service
	allocatingService allocating.Service
	retrievingService retrieving.Service
}

func (s *Server) ConfigureDeposit(ctx context.Context, in *CreateDeposit) (*ResponseDeposit, error) {
	depositRequest, err := createDepositRequestToDomainDeposit(in)
	if err != nil {
		return nil, err
	}

	deposit, err := s.creatingService.ConfigureNewDeposit(ctx, depositRequest)
	validationErr := domain.ErrInvalidData{}
	if errors.As(err, &validationErr) {
		return nil, status.Error(codes.InvalidArgument, validationErr.Error())
	}
	if errors.Is(err, domain.ErrNotFound) {
		return nil, status.Error(codes.NotFound, "not found")
	}
	if err != nil {
		return nil, status.Error(codes.Internal, "internal server error")
	}

	return domainDepositToResponseDeposit(deposit), nil
}

func (s *Server) RetrieveDeposit(ctx context.Context, in *DepositIdentifiers) (*ResponseDeposit, error) {
	clientUUID, err := uuid.Parse(in.ClientID)
	if err != nil || clientUUID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "client ID must be a UUID")
	}

	depositUUID, err := uuid.Parse(in.DepositID)
	if err != nil || depositUUID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "deposit ID must be a UUID")
	}

	deposit, err := s.retrievingService.GetDepositByID(ctx, clientUUID, depositUUID)
	if errors.Is(err, domain.ErrNotFound) {
		return nil, status.Error(codes.NotFound, "not found")
	}
	if err != nil {
		return nil, status.Error(codes.Internal, "internal server error")
	}

	return domainDepositToResponseDeposit(deposit), nil
}

func (s *Server) AllocateReceipt(ctx context.Context, in *CreateReceipt) (*ResponseReceipt, error) {
	clientUUID, err := uuid.Parse(in.DepositIdentifiers.ClientID)
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, "client ID must be a UUID")
	}

	depositUUID, err := uuid.Parse(in.DepositIdentifiers.DepositID)
	if err != nil || depositUUID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "deposit ID must be a UUID")
	}

	receiptRequest, err := createReceiptRequestToDomainReceipt(in)
	if err != nil {
		return nil, err
	}

	receipt, err := s.allocatingService.AllocateReceipt(ctx, receiptRequest, clientUUID, depositUUID)
	if errors.Is(err, domain.ErrNotFound) {
		return nil, status.Error(codes.NotFound, "not found")
	}
	if errors.Is(err, domain.ErrCannotAllocateReceiptFunds) {
		return nil, status.Error(codes.InvalidArgument, "the receipt was rejected because the value of the receipt would overflow the nominal amount of an ISA or SIPP account and no GIA exists to pay the remaining balance into")
	}
	if err != nil {
		return nil, status.Error(codes.Internal, "internal server error")
	}

	return domainReceiptToResponseReceipt(receipt), nil
}

func createDepositRequestToDomainDeposit(d *CreateDeposit) (creating.Deposit, error) {
	clientUUID, err := uuid.Parse(d.ClientID)
	if err != nil || clientUUID == uuid.Nil {
		return creating.Deposit{}, status.Error(codes.InvalidArgument, "client ID must be a UUID")
	}
	pots := []creating.Pot{}
	for _, p := range d.Pots {
		accounts := []creating.Account{}
		for _, a := range p.Accounts {
			t, err := domain.ParseAccountType(a.Type)
			if errors.Is(err, domain.ErrInvalidAccountType) {
				return creating.Deposit{}, status.Error(codes.InvalidArgument, "incorrect account type supplied")
			}
			if err != nil {
				return creating.Deposit{}, status.Error(codes.Internal, "internal server error")
			}
			accounts = append(accounts, creating.Account{
				Type:          t,
				NominalAmount: int(a.NominalAmount),
			})
		}
		pots = append(pots, creating.Pot{
			Name:     p.Name,
			Accounts: accounts,
		})
	}
	return creating.Deposit{
		ClientID: clientUUID,
		Pots:     pots,
	}, nil
}

func createReceiptRequestToDomainReceipt(r *CreateReceipt) (allocating.Receipt, error) {
	potUUID, err := uuid.Parse(r.PotID)
	if err != nil {
		return allocating.Receipt{}, status.Error(codes.InvalidArgument, "pot ID must be a UUID")
	}
	return allocating.Receipt{
		PotID: potUUID,
		Value: int(r.Value),
	}, nil
}

func domainDepositToResponseDeposit(d retrieving.Deposit) *ResponseDeposit {
	pots := []*ResponsePot{}
	for _, p := range d.Pots {
		accounts := []*ResponseAccount{}
		for _, a := range p.Accounts {
			accounts = append(accounts, &ResponseAccount{
				Type:          a.Type.String(),
				NominalAmount: int32(a.NominalAmount),
				Balance:       int32(a.Balance),
			})
		}
		pots = append(pots, &ResponsePot{
			ID:       p.ID.String(),
			Name:     p.Name,
			Accounts: accounts,
		})
	}

	receipts := []*ResponseReceipt{}
	for _, r := range d.Receipts {
		receipts = append(receipts, domainReceiptToResponseReceipt(r))
	}
	return &ResponseDeposit{
		ID:       d.ID.String(),
		ClientID: d.ClientID.String(),
		Pots:     pots,
		Receipts: receipts,
	}
}

func domainReceiptToResponseReceipt(r retrieving.Receipt) *ResponseReceipt {
	allocations := []*ResponseAccountAllocation{}
	for _, a := range r.AccountAllocations {
		allocations = append(allocations, &ResponseAccountAllocation{
			AccountType: a.Type.String(),
			Allocation:  int32(a.Allocation),
		})
	}
	return &ResponseReceipt{
		ID:                 r.ID.String(),
		PotID:              r.ID.String(),
		Value:              int32(r.Value),
		AccountAllocations: allocations,
	}
}

func NewServer(
	creatingService creating.Service,
	allocatingService allocating.Service,
	retrievingService retrieving.Service,
) *grpc.Server {
	s := grpc.NewServer()
	RegisterDepositsServerWithDependencies(
		s,
		creatingService,
		allocatingService,
		retrievingService,
	)
	return s
}

func RegisterDepositsServerWithDependencies(
	s grpc.ServiceRegistrar,
	creatingService creating.Service,
	allocatingService allocating.Service,
	retrievingService retrieving.Service,
) {
	RegisterDepositsServer(s, &Server{
		creatingService:   creatingService,
		allocatingService: allocatingService,
		retrievingService: retrievingService,
	})
}
