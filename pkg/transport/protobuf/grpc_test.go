package protobuf_test

import (
	"context"
	"log"
	"net"
	"testing"
	"time"

	"github.com/google/uuid"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain/allocating"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain/creating"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain/retrieving"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/storage/memory"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/transport/protobuf"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/status"
	"google.golang.org/grpc/test/bufconn"
)

var lis *bufconn.Listener

func bufdialer(context.Context, string) (net.Conn, error) {
	return lis.Dial()
}

func TestGRPC(t *testing.T) {
	// Setup testing server
	repo := memory.NewStore()

	creatingService := creating.NewService(repo)
	allocatingService := allocating.NewService(repo)
	retrievingService := retrieving.NewService(repo)

	lis = bufconn.Listen(1024 * 1024)
	s := grpc.NewServer()
	protobuf.RegisterDepositsServerWithDependencies(
		s,
		creatingService,
		allocatingService,
		retrievingService,
	)

	go func() {
		err := s.Serve(lis)
		if err != nil {
			log.Fatalf("Server exited with error: %v", err)
		}
	}()

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	conn, err := grpc.NewClient(
		"passthrough://bufnet",
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithContextDialer(bufdialer),
	)
	if err != nil {
		t.Fatal(err)
	}
	defer conn.Close()

	c := protobuf.NewDepositsClient(conn)

	// Run Tests
	t.Run("test configure deposit can be called", func(t *testing.T) {
		clientID := uuid.New()
		err := repo.SeedClient(clientID, "test-client")
		if err != nil {
			t.Fatal(err)
		}

		r, err := c.ConfigureDeposit(ctx, &protobuf.CreateDeposit{
			ClientID: clientID.String(),
			Pots: []*protobuf.CreatePot{
				{
					Name: "test pot",
					Accounts: []*protobuf.CreateAccount{
						{
							Type:          "GIA",
							NominalAmount: 30000,
						},
					},
				},
			},
		})
		if err != nil {
			t.Fatal(err)
		}

		if r.ClientID != clientID.String() {
			t.Fatal("unexpected clientID")
		}

		if len(r.Pots) != 1 {
			t.Fatal("incorrect number of pots")
		}
	})

	t.Run("test configure deposit returns InvalidArgument error with invalid account type", func(t *testing.T) {
		clientID := uuid.New()
		err := repo.SeedClient(clientID, "test-client")
		if err != nil {
			t.Fatal(err)
		}

		_, err = c.ConfigureDeposit(ctx, &protobuf.CreateDeposit{
			ClientID: clientID.String(),
			Pots: []*protobuf.CreatePot{
				{
					Name: "test pot",
					Accounts: []*protobuf.CreateAccount{
						{
							Type:          "ZZZ",
							NominalAmount: 30000,
						},
					},
				},
			},
		})
		if err == nil {
			t.Fatal("expected to get InvalidArgument error")
		}
		e, ok := status.FromError(err)
		if !ok {
			t.Fatalf("expected error to be gRPC status error, got %T", err)
		}
		if e.Code() != codes.InvalidArgument {
			t.Fatalf("expected invalid argument error, got %s", err)
		}
	})

	t.Run("test retrieve deposit can be called", func(t *testing.T) {
		clientID := uuid.New()
		err := repo.SeedClient(clientID, "test-client")
		if err != nil {
			t.Fatal(err)
		}

		d, err := c.ConfigureDeposit(ctx, &protobuf.CreateDeposit{
			ClientID: clientID.String(),
			Pots: []*protobuf.CreatePot{
				{
					Name: "test pot",
					Accounts: []*protobuf.CreateAccount{
						{
							Type:          "GIA",
							NominalAmount: 30000,
						},
					},
				},
			},
		})
		if err != nil {
			t.Fatal(err)
		}

		r, err := c.RetrieveDeposit(ctx, &protobuf.DepositIdentifiers{
			ClientID:  clientID.String(),
			DepositID: d.ID,
		})
		if err != nil {
			t.Fatal(err)
		}

		if r.ID != d.ID {
			t.Fatal("expected configured deposit ID to be equal to retrieved deposit ID")
		}
	})

	t.Run("test retrieve non-existant deposit returns not found", func(t *testing.T) {
		clientID := uuid.New()
		err := repo.SeedClient(clientID, "test-client")
		if err != nil {
			t.Fatal(err)
		}

		_, err = c.RetrieveDeposit(ctx, &protobuf.DepositIdentifiers{
			ClientID:  clientID.String(),
			DepositID: uuid.NewString(),
		})
		if err == nil {
			t.Fatal("expected error but did not receive one")
		}

		e, ok := status.FromError(err)
		if !ok {
			t.Fatalf("expected error to be gRPC status error, got %T", err)
		}
		if e.Code() != codes.NotFound {
			t.Fatalf("expected not found error, got %s", err)
		}
	})

	t.Run("test allocate receipt can be called", func(t *testing.T) {
		clientID := uuid.New()
		err := repo.SeedClient(clientID, "test-client")
		if err != nil {
			t.Fatal(err)
		}

		d, err := c.ConfigureDeposit(ctx, &protobuf.CreateDeposit{
			ClientID: clientID.String(),
			Pots: []*protobuf.CreatePot{
				{
					Name: "test pot",
					Accounts: []*protobuf.CreateAccount{
						{
							Type:          "GIA",
							NominalAmount: 30000,
						},
					},
				},
			},
		})
		if err != nil {
			t.Fatal(err)
		}

		_, err = c.AllocateReceipt(ctx, &protobuf.CreateReceipt{
			DepositIdentifiers: &protobuf.DepositIdentifiers{
				ClientID:  clientID.String(),
				DepositID: d.ID,
			},
			PotID: d.Pots[0].ID,
			Value: 10000,
		})
		if err != nil {
			t.Fatal(err)
		}
	})
}
