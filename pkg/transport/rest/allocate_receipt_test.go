package rest_test

import (
	"fmt"
	"net/http"
	"testing"

	"github.com/google/uuid"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain/allocating"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain/creating"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain/retrieving"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/transport/rest"
)

func allocateReceiptTest(t *testing.T) {
	t.Run("test receiving receipt for deposit that doesn't exist returns 404", func(t *testing.T) {
		clientID := makeRandomClient()
		wrongDepositID := uuid.NewString()
		potID := uuid.New()

		j := allocating.Receipt{
			PotID: potID,
			Value: 3000,
		}

		apitester.
			Post(fmt.Sprintf("/deposits/%s/%s/receipts", clientID, wrongDepositID)).
			JSON(j).
			Expect(t).
			Status(http.StatusNotFound).
			End()
	})

	t.Run("test receiving receipt for a pot that doesn't exist on a deposit that does exist returns 404", func(t *testing.T) {
		clientID := makeRandomClient()
		wrongPotID := uuid.New()

		createDepositJSON := creating.Deposit{
			ClientID: clientID,
			Pots: []creating.Pot{
				{
					Name: "test pot",
					Accounts: []creating.Account{
						{Type: domain.GIA, NominalAmount: 30000},
					},
				},
			},
		}

		res := apitester.
			Post("/deposits").
			JSON(createDepositJSON).
			Expect(t).
			Status(http.StatusCreated).
			End()

		d, err := rest.JSONDecode[retrieving.Deposit](res.Response.Body)
		if err != nil {
			t.Fatal(err)
		}

		receiptJSON := allocating.Receipt{
			PotID: wrongPotID,
			Value: 3000,
		}

		apitester.
			Post(fmt.Sprintf("/deposits/%s/%s/receipts", clientID, d.ID)).
			JSON(receiptJSON).
			Expect(t).
			Status(http.StatusNotFound).
			End()
	})

	t.Run("test receiving receipt for the wrong client returns 404", func(t *testing.T) {
		wrongClientID := makeRandomClient()
		clientID := makeRandomClient()

		createDepositJSON := creating.Deposit{
			ClientID: clientID,
			Pots: []creating.Pot{
				{
					Name: "test pot",
					Accounts: []creating.Account{
						{Type: domain.GIA, NominalAmount: 30000},
					},
				},
			},
		}

		res := apitester.
			Post("/deposits").
			JSON(createDepositJSON).
			Expect(t).
			Status(http.StatusCreated).
			End()

		d, err := rest.JSONDecode[retrieving.Deposit](res.Response.Body)
		if err != nil {
			t.Fatal(err)
		}

		receiptJSON := allocating.Receipt{
			PotID: d.Pots[0].ID,
			Value: 3000,
		}

		apitester.
			Post(fmt.Sprintf("/deposits/%s/%s/receipts", wrongClientID, d.ID)).
			JSON(receiptJSON).
			Expect(t).
			Status(http.StatusNotFound).
			End()
	})

	t.Run("test receiving receipt for an existing deposit returns 200", func(t *testing.T) {
		clientID := makeRandomClient()

		createDepositJSON := creating.Deposit{
			ClientID: clientID,
			Pots: []creating.Pot{
				{
					Name: "test pot",
					Accounts: []creating.Account{
						{Type: domain.GIA, NominalAmount: 30000},
					},
				},
			},
		}

		res := apitester.
			Post("/deposits").
			JSON(createDepositJSON).
			Expect(t).
			Status(http.StatusCreated).
			End()

		d, err := rest.JSONDecode[retrieving.Deposit](res.Response.Body)
		if err != nil {
			t.Fatal(err)
		}

		receiptJSON := allocating.Receipt{
			PotID: d.Pots[0].ID,
			Value: 1000,
		}

		apitester.
			Post(fmt.Sprintf("/deposits/%s/%s/receipts", clientID, d.ID)).
			JSON(receiptJSON).
			Expect(t).
			Status(http.StatusCreated).
			End()
	})

	t.Run("test receiving receipt for an existing deposit returns 200 with correct data", func(t *testing.T) {
		clientID := makeRandomClient()

		createDepositJSON := creating.Deposit{
			ClientID: clientID,
			Pots: []creating.Pot{
				{
					Name: "test pot",
					Accounts: []creating.Account{
						{Type: domain.GIA, NominalAmount: 30000},
					},
				},
			},
		}

		res := apitester.
			Post("/deposits").
			JSON(createDepositJSON).
			Expect(t).
			Status(http.StatusCreated).
			End()

		d, err := rest.JSONDecode[retrieving.Deposit](res.Response.Body)
		if err != nil {
			t.Fatal(err)
		}

		receiptJSON := allocating.Receipt{
			PotID: d.Pots[0].ID,
			Value: 1000,
		}

		res = apitester.
			Post(fmt.Sprintf("/deposits/%s/%s/receipts", clientID, d.ID)).
			JSON(receiptJSON).
			Expect(t).
			Status(http.StatusCreated).
			End()

		r, err := rest.JSONDecode[retrieving.Receipt](res.Response.Body)
		if err != nil {
			t.Fatal(err)
		}

		if receiptJSON.Value != r.Value {
			t.Fatalf("incorrect receipt value: expected %d, got %d", receiptJSON.Value, r.Value)
		}
		if len(r.AccountAllocations) != 1 {
			t.Fatalf("incorrect receipt account allocations count: expected %d, got %d", 1, len(r.AccountAllocations))
		}
		if receiptJSON.Value != r.AccountAllocations[0].Allocation {
			t.Fatalf("incorrect account allocation value: expected %d, got %d", receiptJSON.Value, r.AccountAllocations[0].Allocation)
		}
		if domain.GIA.String() != r.AccountAllocations[0].Type.String() {
			t.Fatalf("incorrect allocation account type: expected %s, got %s", domain.GIA.String(), r.AccountAllocations[0].Type.String())
		}
		if receiptJSON.PotID != r.PotID {
			t.Fatalf("incorrect pot ID value: expected %s, got %s", receiptJSON.PotID, r.PotID)
		}
	})

	t.Run("test receiving receipt with high value spreads the allocations correctly", func(t *testing.T) {
		clientID := makeRandomClient()

		createDepositJSON := creating.Deposit{
			ClientID: clientID,
			Pots: []creating.Pot{
				{
					Name: "test pot",
					Accounts: []creating.Account{
						{Type: domain.GIA, NominalAmount: 30000},
						{Type: domain.ISA, NominalAmount: 50000},
						{Type: domain.SIPP, NominalAmount: 100000},
					},
				},
			},
		}

		res := apitester.
			Post("/deposits").
			JSON(createDepositJSON).
			Expect(t).
			Status(http.StatusCreated).
			End()

		d, err := rest.JSONDecode[retrieving.Deposit](res.Response.Body)
		if err != nil {
			t.Fatal(err)
		}

		receiptJSON := allocating.Receipt{
			PotID: d.Pots[0].ID,
			Value: 240000,
		}

		res = apitester.
			Post(fmt.Sprintf("/deposits/%s/%s/receipts", clientID, d.ID)).
			JSON(receiptJSON).
			Expect(t).
			Status(http.StatusCreated).
			End()

		r, err := rest.JSONDecode[retrieving.Receipt](res.Response.Body)
		if err != nil {
			t.Fatal(err)
		}

		if receiptJSON.Value != r.Value {
			t.Fatalf("incorrect receipt value: expected %d, got %d", receiptJSON.Value, r.Value)
		}
		if len(r.AccountAllocations) != 3 {
			t.Fatalf("incorrect receipt account allocations count: expected %d, got %d", 3, len(r.AccountAllocations))
		}
		for _, allocation := range r.AccountAllocations {
			if allocation.Type == domain.GIA && allocation.Allocation != 90_000 {
				t.Fatalf("incorrect GIA allocation, expected 90000, got %d", allocation.Allocation)
			}
			if allocation.Type == domain.ISA && allocation.Allocation != 50_000 {
				t.Fatalf("incorrect ISA allocation, expected 50000, got %d", allocation.Allocation)
			}
			if allocation.Type == domain.SIPP && allocation.Allocation != 100_000 {
				t.Fatalf("incorrect SIPP allocation, expected 100000, got %d", allocation.Allocation)
			}
		}
	})

	t.Run("test receiving receipt with more value than ISA and SIPP can hold returns 400 and does not save", func(t *testing.T) {
		clientID := makeRandomClient()

		createDepositJSON := creating.Deposit{
			ClientID: clientID,
			Pots: []creating.Pot{
				{
					Name: "test pot",
					Accounts: []creating.Account{
						{Type: domain.ISA, NominalAmount: 50000},
						{Type: domain.SIPP, NominalAmount: 100000},
					},
				},
			},
		}

		res := apitester.
			Post("/deposits").
			JSON(createDepositJSON).
			Expect(t).
			Status(http.StatusCreated).
			End()

		d, err := rest.JSONDecode[retrieving.Deposit](res.Response.Body)
		if err != nil {
			t.Fatal(err)
		}

		receiptJSON := allocating.Receipt{
			PotID: d.Pots[0].ID,
			Value: 240000,
		}

		res = apitester.
			Post(fmt.Sprintf("/deposits/%s/%s/receipts", clientID, d.ID)).
			JSON(receiptJSON).
			Expect(t).
			Status(http.StatusBadRequest).
			End()

		res = apitester.
			Get(fmt.Sprintf("/deposits/%s/%s", clientID, d.ID)).
			Expect(t).
			Status(http.StatusOK).
			End()

		responseJSON, err := rest.JSONDecode[retrieving.Deposit](res.Response.Body)
		if err != nil {
			t.Fatal(err)
		}

		if len(responseJSON.Receipts) != 0 {
			t.Fatalf("incorrect number of receipts allocated to deposit: expected 0, got %d", len(responseJSON.Receipts))
		}
		if responseJSON.Pots[0].Accounts[0].Balance != 0 {
			t.Fatalf("incorrect balance for %s account: expected 0, got %d", responseJSON.Pots[0].Accounts[0].Type.String(), responseJSON.Pots[0].Accounts[0].Balance)
		}
		if responseJSON.Pots[0].Accounts[1].Balance != 0 {
			t.Fatalf("incorrect balance for %s account: expected 0, got %d", responseJSON.Pots[0].Accounts[0].Type.String(), responseJSON.Pots[0].Accounts[1].Balance)
		}
	})

	t.Run("test receiving multiple receipts respects the nominal amount maximums", func(t *testing.T) {
		clientID := makeRandomClient()

		createDepositJSON := creating.Deposit{
			ClientID: clientID,
			Pots: []creating.Pot{
				{
					Name: "test pot",
					Accounts: []creating.Account{
						{Type: domain.ISA, NominalAmount: 50000},
						{Type: domain.SIPP, NominalAmount: 100000},
					},
				},
			},
		}

		res := apitester.
			Post("/deposits").
			JSON(createDepositJSON).
			Expect(t).
			Status(http.StatusCreated).
			End()

		d, err := rest.JSONDecode[retrieving.Deposit](res.Response.Body)
		if err != nil {
			t.Fatal(err)
		}

		res = apitester.
			Post(fmt.Sprintf("/deposits/%s/%s/receipts", clientID, d.ID)).
			JSON(allocating.Receipt{
				PotID: d.Pots[0].ID,
				Value: 120000,
			}).
			Expect(t).
			Status(http.StatusCreated).
			End()

		res = apitester.
			Post(fmt.Sprintf("/deposits/%s/%s/receipts", clientID, d.ID)).
			JSON(allocating.Receipt{
				PotID: d.Pots[0].ID,
				Value: 100000,
			}).
			Expect(t).
			Status(http.StatusBadRequest).
			End()
	})
}
