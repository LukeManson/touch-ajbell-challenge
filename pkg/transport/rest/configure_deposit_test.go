package rest_test

import (
	"net/http"
	"testing"

	"github.com/google/uuid"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain/creating"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/transport/rest"
)

func configureDepositTests(t *testing.T) {
	t.Run("test configure new deposit returns 200", func(t *testing.T) {
		clientID := makeRandomClient()
		j := creating.Deposit{
			ClientID: clientID,
			Pots: []creating.Pot{
				{
					Name: "test pot", Accounts: []creating.Account{
						{Type: domain.GIA, NominalAmount: 30000},
					},
				},
			},
		}

		apitester.
			Post("/deposits").
			JSON(j).
			Expect(t).
			Status(http.StatusCreated).
			End()
	})

	t.Run("test configure more than one deposit for the same client returns 200", func(t *testing.T) {
		clientID := makeRandomClient()
		j0 := creating.Deposit{
			ClientID: clientID,
			Pots: []creating.Pot{
				{
					Name: "test pot", Accounts: []creating.Account{
						{Type: domain.GIA, NominalAmount: 30000},
					},
				},
			},
		}

		apitester.
			Post("/deposits").
			JSON(j0).
			Expect(t).
			Status(http.StatusCreated).
			End()

		j1 := creating.Deposit{
			ClientID: clientID,
			Pots: []creating.Pot{
				{
					Name: "test pot2", Accounts: []creating.Account{
						{Type: domain.GIA, NominalAmount: 30000},
					},
				},
			},
		}

		apitester.
			Post("/deposits").
			JSON(j1).
			Expect(t).
			Status(http.StatusCreated).
			End()
	})

	t.Run("test configure new deposit allows multiple different accounts per pot", func(t *testing.T) {
		clientID := makeRandomClient()
		j := creating.Deposit{
			ClientID: clientID,
			Pots: []creating.Pot{
				{
					Name: "test pot", Accounts: []creating.Account{
						{Type: domain.GIA, NominalAmount: 30000},
						{Type: domain.ISA, NominalAmount: 50000},
						{Type: domain.SIPP, NominalAmount: 100000},
					},
				},
			},
		}

		apitester.
			Post("/deposits").
			JSON(j).
			Expect(t).
			Status(http.StatusCreated).
			End()
	})

	t.Run("test configure new deposit returns 400 with invalid account type", func(t *testing.T) {
		clientID := makeRandomClient()
		j := map[string]any{
			"client_id": clientID.String(),
			"pots": []map[string]any{
				{"name": "test pot", "accounts": []map[string]any{
					{"type": "ZZZ", "nominal_amount": 30000},
				}},
			},
		}
		apitester.
			Post("/deposits").
			JSON(j).
			Expect(t).
			Status(http.StatusBadRequest).
			End()
	})

	t.Run("test configure new deposit returns 400 when the client doesn't exist", func(t *testing.T) {
		clientID := uuid.New()
		j := creating.Deposit{
			ClientID: clientID,
			Pots: []creating.Pot{
				{
					Name: "test pot", Accounts: []creating.Account{
						{Type: domain.GIA, NominalAmount: 30000},
					},
				},
			},
		}

		apitester.
			Post("/deposits").
			JSON(j).
			Expect(t).
			Status(http.StatusBadRequest).
			End()
	})

	t.Run("test configure new deposit returns 400 when a nominal value is not a positive", func(t *testing.T) {
		clientID := makeRandomClient()
		j := creating.Deposit{
			ClientID: clientID,
			Pots: []creating.Pot{
				{
					Name: "test pot", Accounts: []creating.Account{
						{Type: domain.GIA, NominalAmount: -30000},
					},
				},
			},
		}

		apitester.
			Post("/deposits").
			JSON(j).
			Expect(t).
			Status(http.StatusBadRequest).
			End()
	})

	t.Run("test configure new deposit returns 400 when an account has multiple of the same types", func(t *testing.T) {
		clientID := makeRandomClient()
		j := creating.Deposit{
			ClientID: clientID,
			Pots: []creating.Pot{
				{
					Name: "test pot", Accounts: []creating.Account{
						{Type: domain.GIA, NominalAmount: 30000},
						{Type: domain.GIA, NominalAmount: 20000},
					},
				},
			},
		}

		apitester.
			Post("/deposits").
			JSON(j).
			Expect(t).
			Status(http.StatusBadRequest).
			End()
	})

	t.Run("test configure new deposit returns 400 when a pot has no accounts", func(t *testing.T) {
		clientID := makeRandomClient()
		j := creating.Deposit{
			ClientID: clientID,
			Pots: []creating.Pot{
				{
					Name: "test pot", Accounts: []creating.Account{},
				},
			},
		}

		apitester.
			Post("/deposits").
			JSON(j).
			Expect(t).
			Status(http.StatusBadRequest).
			End()
	})

	t.Run("test configure new deposit returns 400 when a pot has an empty name", func(t *testing.T) {
		clientID := makeRandomClient()
		j := creating.Deposit{
			ClientID: clientID,
			Pots: []creating.Pot{
				{
					Name: "", Accounts: []creating.Account{
						{Type: domain.GIA, NominalAmount: 30000},
					},
				},
			},
		}

		apitester.
			Post("/deposits").
			JSON(j).
			Expect(t).
			Status(http.StatusBadRequest).
			End()
	})

	t.Run("test configure new deposit returns 400 when a pot has an name longer than 64 chars", func(t *testing.T) {
		clientID := makeRandomClient()
		j := creating.Deposit{
			ClientID: clientID,
			Pots: []creating.Pot{
				{
					Name: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
					Accounts: []creating.Account{
						{Type: domain.GIA, NominalAmount: 30000},
					},
				},
			},
		}

		apitester.
			Post("/deposits").
			JSON(j).
			Expect(t).
			Status(http.StatusBadRequest).
			End()
	})

	t.Run("test configure new deposit returns the correct json data", func(t *testing.T) {
		clientID := makeRandomClient()
		j := creating.Deposit{
			ClientID: clientID,
			Pots: []creating.Pot{
				{
					Name: "test pot",
					Accounts: []creating.Account{
						{Type: domain.GIA, NominalAmount: 30000},
					},
				},
			},
		}

		res := apitester.
			Post("/deposits").
			JSON(j).
			Expect(t).
			Status(http.StatusCreated).
			End()

		responseData, err := rest.JSONDecode[map[string]any](res.Response.Body)
		if err != nil {
			t.Fatal(err)
		}

		if responseData["client_id"].(string) != clientID.String() {
			t.Fatalf("incorrect client_id: expected %s, got %s", responseData["client_id"], clientID)
		}

		idStr, ok := responseData["id"].(string)
		if !ok {
			t.Fatalf("incorrect id format: expected string, got %T", responseData["id"])
		}

		_, err = uuid.Parse(idStr)
		if err != nil {
			t.Fatalf("deposit id format is expected to be UUID: got %s", idStr)
		}

		pots, ok := responseData["pots"].([]any)
		if !ok {
			t.Fatalf("incorrect pots type: expected []any, got %T", responseData["pots"])
		}

		if len(pots) != 1 {
			t.Fatalf("incorrect number of pots returned: expected 1, got %d", len(pots))
		}

		pot, ok := pots[0].(map[string]any)
		if !ok {
			t.Fatalf("incorrect pot type: expected map[string]any, got %T", pots[0])
		}

		name, ok := pot["name"].(string)
		if !ok {
			t.Fatalf("incorrect type for pot name: expected string, got %T", pot["name"])
		}

		if name != j.Pots[0].Name {
			t.Fatalf("incorrect pot name: expected %s, got %s", j.Pots[0].Name, name)
		}

		idStr, ok = pot["id"].(string)
		if !ok {
			t.Fatalf("incorrect type for pot id: expected string, got %T", pot["id"])
		}

		_, err = uuid.Parse(idStr)
		if err != nil {
			t.Fatalf("pot id format is expected to be UUID: got %s", idStr)
		}

		accounts, ok := pot["accounts"].([]any)
		if !ok {
			t.Fatalf("incorrect accounts type: expected []any, got %T", pot["accounts"])
		}

		if len(accounts) != 1 {
			t.Fatalf("incorrect number of accounts: expected 1, got %d", len(accounts))
		}

		account, ok := accounts[0].(map[string]any)
		if !ok {
			t.Fatalf("incorrect acount (golang) type: expected map[string]any, got %T", pots[0])
		}

		typeStr, ok := account["type"].(string)
		if !ok {
			t.Fatalf("incorrect account type type: expected string, got %T", account["type"])
		}

		expectedAccountTypeValue := j.Pots[0].Accounts[0].Type.String()
		if typeStr != expectedAccountTypeValue {
			t.Fatalf("incorrect account type value: expected %s, got %s", typeStr, expectedAccountTypeValue)
		}

		// JSON only has number types, not int and float, so all come through as float64 annoyingly
		nominalAmountFloat, ok := account["nominal_amount"].(float64)
		if !ok {
			t.Fatalf("incorrect account nominal value type: expected float64, got %T", account["nominal_amount"])
		}

		// This check will also check to make sure the float we parsed was an integer since they must be equal
		expectedAccountNominalAmount := float64(j.Pots[0].Accounts[0].NominalAmount)
		if nominalAmountFloat != expectedAccountNominalAmount {
			t.Fatalf("incorrect nominal value: expected %f, got %f", expectedAccountNominalAmount, nominalAmountFloat)
		}

		balance, ok := account["balance"].(float64)
		if !ok {
			t.Fatalf("incorrect account balance type: expected float64, got %T", account["nominal_amount"])
		}

		if balance != 0 {
			t.Fatalf("incorrect nominal value: expected 0, got %f", balance)
		}
	})
}
