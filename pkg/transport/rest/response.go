package rest

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func respond(w http.ResponseWriter, statusCode int, responseData any) {
	response, err := json.Marshal(responseData)
	if err != nil {
		panic(fmt.Errorf("failed to marshal value into json: %w", err))
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	_, err = w.Write(response)
	if err != nil {
		panic(fmt.Errorf("failed to write json to buffer: %w", err))
	}
}

func respondWithProblem(w http.ResponseWriter, statusCode int, problem string) {
	data := map[string]string{
		"error_message": problem,
	}

	respond(w, statusCode, data)
}

func respondWithNotFound(w http.ResponseWriter) {
	respondWithProblem(w, http.StatusNotFound, "not found")
}

func respondWithServerError(w http.ResponseWriter) {
	respondWithProblem(w, http.StatusInternalServerError, "internal server error")
}
