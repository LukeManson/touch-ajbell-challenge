package rest

import (
	"errors"
	"fmt"
	"log"
	"net/http"

	"github.com/google/uuid"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain/allocating"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain/creating"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain/retrieving"
)

func NewMux(
	creatingService creating.Service,
	retrievingService retrieving.Service,
	allocatingService allocating.Service,
) http.Handler {
	mux := http.NewServeMux()

	for path, handler := range GenerateRoutes(
		creatingService,
		retrievingService,
		allocatingService,
	) {
		mux.HandleFunc(path, handler)
	}

	return mux
}

func GenerateRoutes(
	creatingService creating.Service,
	retrievingService retrieving.Service,
	allocatingService allocating.Service,
) map[string]http.HandlerFunc {
	return map[string]http.HandlerFunc{
		post("/deposits"):                                 configureDepositHandler(creatingService),
		get("/deposits/{clientID}/{depositID}"):           retrieveDepositHandler(retrievingService),
		post("/deposits/{clientID}/{depositID}/receipts"): createReceiptHandler(allocatingService),
	}
}

func configureDepositHandler(service creating.Service) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		depositRequest, err := JSONDecode[creating.Deposit](r.Body)
		if err != nil {
			respondWithProblem(w, http.StatusBadRequest, "incorrect json structure or format")
			return
		}

		deposit, err := service.ConfigureNewDeposit(ctx, depositRequest)
		validationErr := domain.ErrInvalidData{}
		if errors.As(err, &validationErr) {
			respondWithProblem(w, http.StatusBadRequest, validationErr.Error())
			return
		}
		if errors.Is(err, domain.ErrUnknownClientID) {
			respondWithProblem(w, http.StatusBadRequest, "unknown client")
			return
		}
		if err != nil {
			log.Printf("error: unexpected error occurred: %s", err)
			respondWithServerError(w)
			return
		}

		respond(w, http.StatusCreated, deposit)
	}
}

func retrieveDepositHandler(service retrieving.Service) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		clientID, err := uuid.Parse(r.PathValue("clientID"))
		if err != nil || clientID == uuid.Nil {
			respondWithProblem(w, http.StatusBadRequest, "invalid client id: must be a uuid")
			return
		}
		depositID, err := uuid.Parse(r.PathValue("depositID"))
		if err != nil || depositID == uuid.Nil {
			respondWithProblem(w, http.StatusBadRequest, "invalid deposit id: must be a uuid")
			return
		}

		deposit, err := service.GetDepositByID(ctx, clientID, depositID)
		if errors.Is(err, domain.ErrNotFound) {
			respondWithNotFound(w)
			return
		}

		respond(w, http.StatusOK, deposit)
	}
}

func createReceiptHandler(service allocating.Service) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		clientID, err := uuid.Parse(r.PathValue("clientID"))
		if err != nil || clientID == uuid.Nil {
			respondWithProblem(w, http.StatusBadRequest, "invalid client id: must be a uuid")
			return
		}

		depositID, err := uuid.Parse(r.PathValue("depositID"))
		if err != nil || depositID == uuid.Nil {
			respondWithProblem(w, http.StatusBadRequest, "invalid deposit id: must be a uuid")
			return
		}

		receiptRequest, err := JSONDecode[allocating.Receipt](r.Body)
		if err != nil {
			respondWithProblem(w, http.StatusBadRequest, "incorrect json structure or format")
			return
		}

		responseReceipt, err := service.AllocateReceipt(ctx, receiptRequest, clientID, depositID)
		if errors.Is(err, domain.ErrNotFound) {
			respondWithNotFound(w)
			return
		}
		if errors.Is(err, domain.ErrCannotAllocateReceiptFunds) {
			respondWithProblem(w, http.StatusBadRequest, "could not allocate funds, check the value does not overflow the remaining nominal remaining value of the ISA and SIPP accounts or have a GIA account to allow overflow")
			return
		}
		if err != nil {
			log.Printf("error: unexpected error occurred: %s", err)
			respondWithServerError(w)
			return
		}

		respond(w, http.StatusCreated, responseReceipt)
	}
}

func post(path string) string {
	return fmt.Sprintf("%s %s", http.MethodPost, path)
}

func get(path string) string {
	return fmt.Sprintf("%s %s", http.MethodGet, path)
}
