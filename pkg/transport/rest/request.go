package rest

import (
	"encoding/json"
	"io"
)

func JSONDecode[T any](r io.Reader) (T, error) {
	var v T
	decoder := json.NewDecoder(r)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(&v)
	return v, err
}
