package rest_test

import (
	"fmt"
	"net/http"
	"testing"

	"github.com/google/uuid"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain/allocating"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain/creating"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain/retrieving"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/transport/rest"
)

func retrieveDepositTests(t *testing.T) {
	t.Run("test retrieve existing deposit returns correct format", func(t *testing.T) {
		clientID := makeRandomClient()
		j := creating.Deposit{
			ClientID: clientID,
			Pots: []creating.Pot{
				{
					Name: "test pot",
					Accounts: []creating.Account{
						{Type: domain.GIA, NominalAmount: 30000},
					},
				},
			},
		}

		createRes := apitester.
			Post("/deposits").
			JSON(j).
			Expect(t).
			Status(http.StatusCreated).
			End()

		createResponseData, err := rest.JSONDecode[map[string]any](createRes.Response.Body)
		if err != nil {
			t.Fatal(err)
		}

		idStr, ok := createResponseData["id"].(string)
		if !ok {
			t.Fatalf("incorrect ID type on createResponse: expected string, got %T", createResponseData["id"])
		}

		getRes := apitester.
			Get(fmt.Sprintf("/deposits/%s/%s", clientID, idStr)).
			Expect(t).
			Status(http.StatusOK).
			End()

		responseData, err := rest.JSONDecode[map[string]any](getRes.Response.Body)
		if err != nil {
			t.Fatal(err)
		}

		if responseData["client_id"].(string) != clientID.String() {
			t.Fatalf("incorrect client_id: expected %s, got %s", responseData["client_id"], clientID)
		}

		idStr, ok = responseData["id"].(string)
		if !ok {
			t.Fatalf("incorrect id format: expected string, got %T", responseData["id"])
		}

		_, err = uuid.Parse(idStr)
		if err != nil {
			t.Fatalf("deposit id format is expected to be UUID: got %s", idStr)
		}

		receipts, ok := responseData["receipts"].([]any)
		if !ok {
			t.Fatalf("incorrect receipts type: expected []any, got %T", responseData["receipts"])
		}

		if len(receipts) != 0 {
			t.Fatalf("incorrect number of receipts returned: expected 0, got %d", len(receipts))
		}

		pots, ok := responseData["pots"].([]any)
		if !ok {
			t.Fatalf("incorrect pots type: expected []any, got %T", responseData["pots"])
		}

		if len(pots) != 1 {
			t.Fatalf("incorrect number of pots returned: expected 1, got %d", len(pots))
		}

		pot, ok := pots[0].(map[string]any)
		if !ok {
			t.Fatalf("incorrect pot type: expected map[string]any, got %T", pots[0])
		}

		name, ok := pot["name"].(string)
		if !ok {
			t.Fatalf("incorrect type for pot name: expected string, got %T", pot["name"])
		}

		if name != j.Pots[0].Name {
			t.Fatalf("incorrect pot name: expected %s, got %s", j.Pots[0].Name, name)
		}

		idStr, ok = pot["id"].(string)
		if !ok {
			t.Fatalf("incorrect type for pot id: expected string, got %T", pot["id"])
		}

		_, err = uuid.Parse(idStr)
		if err != nil {
			t.Fatalf("pot id format is expected to be UUID: got %s", idStr)
		}

		accounts, ok := pot["accounts"].([]any)
		if !ok {
			t.Fatalf("incorrect accounts type: expected []any, got %T", pot["accounts"])
		}

		if len(accounts) != 1 {
			t.Fatalf("incorrect number of accounts: expected 1, got %d", len(accounts))
		}

		account, ok := accounts[0].(map[string]any)
		if !ok {
			t.Fatalf("incorrect acount (golang) type: expected map[string]any, got %T", pots[0])
		}

		typeStr, ok := account["type"].(string)
		if !ok {
			t.Fatalf("incorrect account type type: expected string, got %T", account["type"])
		}

		expectedAccountTypeValue := j.Pots[0].Accounts[0].Type.String()
		if typeStr != expectedAccountTypeValue {
			t.Fatalf("incorrect account type value: expected %s, got %s", typeStr, expectedAccountTypeValue)
		}

		// JSON only has number types, not int and float, so all come through as float64 annoyingly
		nominalAmountFloat, ok := account["nominal_amount"].(float64)
		if !ok {
			t.Fatalf("incorrect account nominal value type: expected int, got %T", account["nominal_amount"])
		}

		// This check will also check to make sure the float we parsed was an integer since they must be equal
		expectedAccountNominalAmount := float64(j.Pots[0].Accounts[0].NominalAmount)
		if nominalAmountFloat != expectedAccountNominalAmount {
			t.Fatalf("incorrect nominal value: expected %f, got %f", expectedAccountNominalAmount, nominalAmountFloat)
		}

		balance, ok := account["balance"].(float64)
		if !ok {
			t.Fatalf("incorrect account balance type: expected float64, got %T", account["nominal_amount"])
		}

		if balance != 0 {
			t.Fatalf("incorrect nominal value: expected 0, got %f", balance)
		}
	})

	t.Run("test retrieve non-existant deposit returns 404", func(t *testing.T) {
		clientID := makeRandomClient()
		idStr := uuid.NewString()

		apitester.
			Get(fmt.Sprintf("/deposits/%s/%s", clientID, idStr)).
			Expect(t).
			Status(http.StatusNotFound).
			End()
	})

	t.Run("test retrieve existant deposit from wrong client returns 404", func(t *testing.T) {
		clientID := makeRandomClient()
		j := creating.Deposit{
			ClientID: clientID,
			Pots: []creating.Pot{
				{
					Name: "test pot",
					Accounts: []creating.Account{
						{Type: domain.GIA, NominalAmount: 30000},
					},
				},
			},
		}

		createRes := apitester.
			Post("/deposits").
			JSON(j).
			Expect(t).
			Status(http.StatusCreated).
			End()

		createResponseData, err := rest.JSONDecode[map[string]any](createRes.Response.Body)
		if err != nil {
			t.Fatal(err)
		}

		idStr, ok := createResponseData["id"].(string)
		if !ok {
			t.Fatalf("incorrect ID type on createResponse: expected string, got %T", createResponseData["id"])
		}

		wrongClientID := uuid.NewString()

		apitester.
			Get(fmt.Sprintf("/deposits/%s/%s", wrongClientID, idStr)).
			Expect(t).
			Status(http.StatusNotFound).
			End()
	})

	t.Run("test retreiving deposit after allocating receipt returns correct data", func(t *testing.T) {
		clientID := makeRandomClient()

		createDepositJSON := creating.Deposit{
			ClientID: clientID,
			Pots: []creating.Pot{
				{
					Name: "test pot",
					Accounts: []creating.Account{
						{Type: domain.GIA, NominalAmount: 30000},
					},
				},
			},
		}

		res := apitester.
			Post("/deposits").
			JSON(createDepositJSON).
			Expect(t).
			Status(http.StatusCreated).
			End()

		d, err := rest.JSONDecode[retrieving.Deposit](res.Response.Body)
		if err != nil {
			t.Fatal(err)
		}

		receiptJSON := allocating.Receipt{
			PotID: d.Pots[0].ID,
			Value: 1000,
		}

		res = apitester.
			Post(fmt.Sprintf("/deposits/%s/%s/receipts", clientID, d.ID)).
			JSON(receiptJSON).
			Expect(t).
			Status(http.StatusCreated).
			End()

		res = apitester.
			Get(fmt.Sprintf("/deposits/%s/%s", clientID, d.ID)).
			Expect(t).
			Status(http.StatusOK).
			End()

		responseData, err := rest.JSONDecode[retrieving.Deposit](res.Response.Body)
		if err != nil {
			t.Fatal(err)
		}

		receipts := responseData.Receipts
		if len(receipts) != 1 {
			t.Fatalf("incorrect number of receipts: expected 1, got %d", len(receipts))
		}

		if receiptJSON.Value != receipts[0].Value {
			t.Fatalf("incorrect receipt value: expected %d, got %d", receiptJSON.Value, receipts[0].Value)
		}
		if len(receipts[0].AccountAllocations) != 1 {
			t.Fatalf("incorrect receipt account allocations count: expected %d, got %d", 1, len(receipts[0].AccountAllocations))
		}
		if receiptJSON.Value != receipts[0].AccountAllocations[0].Allocation {
			t.Fatalf("incorrect account allocation value: expected %d, got %d", receiptJSON.Value, receipts[0].AccountAllocations[0].Allocation)
		}
		if domain.GIA.String() != receipts[0].AccountAllocations[0].Type.String() {
			t.Fatalf("incorrect allocation account type: expected %s, got %s", domain.GIA.String(), receipts[0].AccountAllocations[0].Type.String())
		}
		if receiptJSON.PotID != receipts[0].PotID {
			t.Fatalf("incorrect pot ID value: expected %s, got %s", receipts[0].PotID, receipts[0].PotID)
		}

		if receiptJSON.Value != responseData.Pots[0].Accounts[0].Balance {
			t.Fatalf("incorrect account balance: expected %d, got %d", receiptJSON.Value, responseData.Pots[0].Accounts[0].Balance)
		}
	})
}
