package rest_test

import (
	"testing"

	"github.com/google/uuid"
	"github.com/steinfletcher/apitest"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain/allocating"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain/creating"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain/retrieving"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/storage/memory"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/transport/rest"
)

var (
	apitester    *apitest.APITest
	clientSeeder func(id uuid.UUID, name string) error
)

func TestMain(m *testing.M) {
	m.Run()
}

// Create a random client per test so we can run tests in parallel and not affect each other.
func makeRandomClient() uuid.UUID {
	clientID := uuid.New()
	err := clientSeeder(clientID, "test")
	if err != nil {
		panic(err)
	}
	return clientID
}

func TestAPI(t *testing.T) {
	// Initialise dependencies
	repo := memory.NewStore()
	creatingService := creating.NewService(repo)
	retrievingService := retrieving.NewService(repo)
	allocatingService := allocating.NewService(repo)

	// Create a test mux
	mux := rest.NewMux(creatingService, retrievingService, allocatingService)

	// Create test tools
	clientSeeder = repo.SeedClient
	apitester = apitest.New().HandlerFunc(mux.ServeHTTP)

	// Run tests
	configureDepositTests(t)
	retrieveDepositTests(t)
	allocateReceiptTest(t)
}
