# Submission for the Touch/AJBell Coding Challenge - Luke Manson

## Dependencies

- Go v1.22
- cmake (optional)

## Installation

```bash
# Clone the repository
git clone https://gitlab.com/LukeManson/touch-ajbell-challenge.git

# Change to the project directory
cd touch-ajbell-challenge

# Install dependencies
go mod tidy
```

## Usage

```bash
# If you have make installed, you can use the provided Makefile which contains useful commands
make run/rest # This will build and run the REST server
make run/grpc # This will build and run the gRPC server

# If you do not have make installed, you can run the app using Go directly
go run ./cmd/rest/main.go
go run ./cmd/grpc/main.go
```

When the server runs, you should get a log output similar to the following:

```
2024/06/15 13:47:09 seeded client with ID = f357d35a-679a-4ec5-9a22-ae7bb8354015
2024/06/15 13:47:09 seeded client with ID = d5b60381-f4bd-40eb-be9d-3844e46c0109
2024/06/15 13:47:09 seeded client with ID = 3d7ac89c-c508-4ab4-9428-a9e00c2483d3
2024/06/15 13:47:09 server listening at :8080
```

Three client IDs are randomly generated when the service starts. You can copy these from the logs of your running application to use them in rest/gRPC requests to the service.

## Available Endpoints

For the rest API, an openAPI specification is included in the repository. A GUI version of this specification is available [on GitLab](https://gitlab.com/LukeManson/touch-ajbell-challenge/-/blob/master/openapi.yaml).

## Assumptions and Limitations

- A receipt can only be allocated to one pot, specified by ID in the configure deposit request data.
- When creating a receipt, the request data does not specify which accounts to allocate funds to; this is decided automatically by the service.
- When a receipt is received, the accounts are allocated in order of assumed tax advantage:
  - ISA
  - SIPP
  - GIA
- The system does not include an authentication mechanism. It is assumed that this service would not be responsible for authenticating requests.
- The system includes a list of clients, which it checks against as a very simple authorisation system.
- All monetary values are handled as integers. Currencies are not explicitly defined in the application, allowing values to be represented in any precision of any currency (e.g. pence). This approach avoids complications with floating-point precision. If decimal places are needed, it may be preferable to pass these values as strings and use a currency package for calculations, such as [Bojanz's Currency package](https://pkg.go.dev/github.com/bojanz/currency).

## Architecture

### Design Pattern

The application has been designed using a form of the hexagonal architecture design pattern. The primary reason for choosing this design was to enable the potential switching out of the storage system (initially implemented as an in-memory store) with a relational database system if needed. The hexagonal architecture pattern facilitates this by decoupling the domain/business logic from the ports/adapters/drivers using Go interfaces to define the dependency requirements of the service. Similarly, the REST API is decoupled from the domain, allowing for alternative entry points to the application.

### Domain Packages

The data structures used by each endpoint are similar but have some distinct differences. For example, creating a deposit doesn't require a deposit ID, pot IDs, or receipts, but these are needed when retrieving the deposit. For this reason, the domain logic is split into three packages:

- creating
- allocating
- retrieving

The retrieving package holds the full types with all fields. The other two have specific data structures that hold only the data used to make the request.

## Extensions Considered

I had two extensions to the MVP build that I considered implementing but only had time to implement one. These improvements were:

1. Using a relational database.
2. Implementing gRPC methods.

Both of these technologies were mentioned in the job specification. I decided to implement the gRPC methods because I have wanted to learn how they work for a long time. Since gRPC is not present on my CV, I thought it would be beneficial to demonstrate my ability to implement it. Conversely, I have many years of database experience on my CV and can showcase that expertise during the technical interview if needed.

Since I was low on time, and the integration tests for the rest endpoints had fairly good coverage of the business logic, I only created some very basic tests for the gRPC endpoints to ensure they were sending and receiving information and a couple of error cases.

## Third-Party Libraries

I have tried to keep third-party dependencies to a minimum. The following dependencies are used in the application:

- Google's UUID package: Used to generate and parse random (V4) UUIDs for the data store.
  - Documentation: https://pkg.go.dev/github.com/google/uuid@v1.6.0
- Google's gRPC package: Required for implementing gRPC endpoints.
  - Documentation: https://pkg.go.dev/google.golang.org/grpc@v1.64.0
- Google's Protobuf package: Required for protocol buffer support in gRPC.
  - Documentation: https://pkg.go.dev/google.golang.org/protobuf@v1.34.2
- Steinfletcher's Apitest package: Used for running rest integration tests by simplifying the making, validating, and testing of HTTP requests.
  - https://pkg.go.dev/github.com/steinfletcher/apitest@v1.5.16

## Future Improvements

- Relational database: The in memory store has many limitations and is only really appropriate for demonstration purposes. Some limitations of the in-memory system are:
  - No transactions/rollbacks which would have been useful for receipt allocations.
  - No persistence if the server is shut down.
  - Multiple instances of the service cannot share data since the data store is unique per instance.
