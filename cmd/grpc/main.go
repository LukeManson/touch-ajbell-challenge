package main

import (
	"log"
	"net"

	"github.com/google/uuid"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain/allocating"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain/creating"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain/retrieving"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/storage/memory"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/transport/protobuf"
)

func main() {
	repo := memory.NewStore()

	// Seed some dummy client data for demo purposes

	cID0 := uuid.New()
	err := repo.SeedClient(cID0, "Alice")
	if err != nil {
		panic(err)
	}
	log.Printf("seeded client with ID = %s\n", cID0)

	cID1 := uuid.New()
	err = repo.SeedClient(cID1, "Bob")
	if err != nil {
		panic(err)
	}
	log.Printf("seeded client with ID = %s\n", cID1)

	cID2 := uuid.New()
	err = repo.SeedClient(cID2, "Charles")
	if err != nil {
		panic(err)
	}
	log.Printf("seeded client with ID = %s\n", cID2)

	creatingService := creating.NewService(repo)
	allocatingService := allocating.NewService(repo)
	retrievingService := retrieving.NewService(repo)

	port := ":50051"
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := protobuf.NewServer(creatingService, allocatingService, retrievingService)
	log.Println("listening on " + port)
	err = s.Serve(lis)
	if err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
