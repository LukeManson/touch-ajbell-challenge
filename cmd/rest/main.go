package main

import (
	"log"
	"net/http"

	"github.com/google/uuid"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain/allocating"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain/creating"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/domain/retrieving"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/storage/memory"
	"gitlab.com/LukeManson/touch-ajbell-challenge/pkg/transport/rest"
)

func main() {
	repo := memory.NewStore()

	// Seed some dummy client data for demo purposes

	cID0 := uuid.New()
	err := repo.SeedClient(cID0, "Alice")
	if err != nil {
		panic(err)
	}
	log.Printf("seeded client with ID = %s\n", cID0)

	cID1 := uuid.New()
	err = repo.SeedClient(cID1, "Bob")
	if err != nil {
		panic(err)
	}
	log.Printf("seeded client with ID = %s\n", cID1)

	cID2 := uuid.New()
	err = repo.SeedClient(cID2, "Charles")
	if err != nil {
		panic(err)
	}
	log.Printf("seeded client with ID = %s\n", cID2)

	creatingService := creating.NewService(repo)
	retrievingService := retrieving.NewService(repo)
	allocatingService := allocating.NewService(repo)

	mux := rest.NewMux(creatingService, retrievingService, allocatingService)

	port := ":8080"
	log.Println("server listening at " + port)
	log.Fatal(http.ListenAndServe(port, mux))
}
