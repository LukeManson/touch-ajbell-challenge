REST_PACKAGE_PATH := ./cmd/rest/main.go
REST_BINARY_NAME := rest

GRPC_PACKAGE_PATH := ./cmd/grpc/main.go
GRPC_BINARY_NAME := grpc

## help: print this help message
.PHONY: help
help:
	@echo 'Usage:'
	@sed -n 's/^##//p' ${MAKEFILE_LIST} | column -t -s ':' |  sed -e 's/^/ /'

## tidy: format code and tidy modfile
.PHONY: tidy
tidy:
	go fmt ./...
	go mod tidy -v

## audit: run quality control checks
.PHONY: audit
audit:
	go mod verify
	go vet ./...
	go run honnef.co/go/tools/cmd/staticcheck@latest -checks=all,-ST1000,-U1000 ./...
	go run golang.org/x/vuln/cmd/govulncheck@latest ./...
	go test -race -buildvcs -vet=off ./...

## test: run all tests
.PHONY: test
test:
	go test -v -race -buildvcs -p 8 ./...

## build grpc methods
.PHONY: build/grpc
build/grpc:
	protoc --go_out=. --go_opt=paths=source_relative \
    --go-grpc_out=. --go-grpc_opt=paths=source_relative \
		pkg/transport/protobuf/deposit.proto	
	@echo "Building binary..."
	@go build -o=./tmp/bin/${GRPC_BINARY_NAME} ${GRPC_PACKAGE_PATH}

## build: build the application
.PHONY: build/rest
build/rest:
	@echo "Building binary..."
	@go build -o=./tmp/bin/${REST_BINARY_NAME} ${REST_PACKAGE_PATH}

## run: run the rest server
.PHONY: run/rest
run/rest: build/rest
	./tmp/bin/${REST_BINARY_NAME}

## run: run the grpc server
.PHONY: run/grpc
run/grpc: build/grpc
	./tmp/bin/${GRPC_BINARY_NAME}

## run/live: run the application with reloading on file changes
.PHONY: run/live
run/live:
	go run github.com/cosmtrek/air@v1.52.1 \
		--build.cmd "make build" --build.bin "./tmp/bin/${REST_BINARY_NAME}" --build.delay "100" \
		--build.include_ext "go" \
		--misc.clean_on_exit "true"
